import logo from './logo.svg';
import './App.css';
import RepForm from './components/rep-form'


function App() {
  return (
    <div className="App" style={{'margin': 50}}>
      <h1 style={{'textAlign': 'left', 'color': '#2AABEF'}}>Who's my Representative?</h1>
      <RepForm />
    </div>
  );
}

export default App;
