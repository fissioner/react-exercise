import RepsList from './reps';

import * as React from 'react';
import { FormControl, InputLabel, Select, MenuItem, Grid, Button } from '@mui/material/';


const RepForm = () => {
    const [politicianType, setPoliticianType] = React.useState('representatives');
    const [usState, setUsState] = React.useState('UT');
    const [reps, setReps] = React.useState([]);

    const changePoliticianType = (event) => {
        setPoliticianType(event.target.value);
    };
    const changeUsState = (event) => {
        setUsState(event.target.value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        (async () => {
            const data = await fetch(`http://localhost:8080/${politicianType}/${usState}`)
                .then(res => res.json())
                .catch(err => err);
            setReps(data['results'].map(rep => rep))
        })();
      };

    return (
    <div>
        <form  onSubmit={handleSubmit} style={{"width": "300px"}} >
            <Grid item xs={12}><h3>Select from the options below.</h3></Grid>
            <Grid container>
                <FormControl fullWidth>
                    <InputLabel >Politician Type</InputLabel>
                    <Select
                        value={politicianType}
                        label="politician type"
                        onChange={changePoliticianType}
                    >
                        <MenuItem value={'representatives'}>Representatives</MenuItem>
                        <MenuItem value={'senators'}>Senators</MenuItem>
                    </Select>
                </FormControl>
            </ Grid>
            <br/>
            <Grid container>
                <FormControl fullWidth>
                    <InputLabel >State</InputLabel>
                    <Select
                        value={usState}
                        label="state"
                        onChange={changeUsState}
                    >
                        <MenuItem value={'AL'}>AL</MenuItem>
                        <MenuItem value={'AK'}>AK</MenuItem>
                        <MenuItem value={'AZ'}>AZ</MenuItem>
                        <MenuItem value={'AR'}>AR</MenuItem>
                        <MenuItem value={'CA'}>CA</MenuItem>
                        <MenuItem value={'CZ'}>CZ</MenuItem>
                        <MenuItem value={'CO'}>CO</MenuItem>
                        <MenuItem value={'CT'}>CT</MenuItem>
                        <MenuItem value={'DE'}>DE</MenuItem>
                        <MenuItem value={'DC'}>DC</MenuItem>
                        <MenuItem value={'FL'}>FL</MenuItem>
                        <MenuItem value={'GA'}>GA</MenuItem>
                        <MenuItem value={'GU'}>GU</MenuItem>
                        <MenuItem value={'HI'}>HI</MenuItem>
                        <MenuItem value={'ID'}>ID</MenuItem>
                        <MenuItem value={'IL'}>IL</MenuItem>
                        <MenuItem value={'IN'}>IN</MenuItem>
                        <MenuItem value={'IA'}>IA</MenuItem>
                        <MenuItem value={'KS'}>KS</MenuItem>
                        <MenuItem value={'KY'}>KY</MenuItem>
                        <MenuItem value={'LA'}>LA</MenuItem>
                        <MenuItem value={'ME'}>ME</MenuItem>
                        <MenuItem value={'MD'}>MD</MenuItem>
                        <MenuItem value={'MA'}>MA</MenuItem>
                        <MenuItem value={'MI'}>MI</MenuItem>
                        <MenuItem value={'MO'}>MO</MenuItem>
                        <MenuItem value={'MT'}>MT</MenuItem>
                        <MenuItem value={'NE'}>NE</MenuItem>
                        <MenuItem value={'NV'}>NV</MenuItem>
                        <MenuItem value={'NH'}>NH</MenuItem>
                        <MenuItem value={'NJ'}>NJ</MenuItem>
                        <MenuItem value={'NM'}>NM</MenuItem>
                        <MenuItem value={'NY'}>NY</MenuItem>
                        <MenuItem value={'NC'}>NC</MenuItem>
                        <MenuItem value={'ND'}>ND</MenuItem>
                        <MenuItem value={'OH'}>OH</MenuItem>
                        <MenuItem value={'OK'}>OK</MenuItem>
                        <MenuItem value={'OR'}>OR</MenuItem>
                        <MenuItem value={'PA'}>PA</MenuItem>
                        <MenuItem value={'PR'}>PR</MenuItem>
                        <MenuItem value={'RI'}>RI</MenuItem>
                        <MenuItem value={'SC'}>SC</MenuItem>
                        <MenuItem value={'SD'}>SD</MenuItem>
                        <MenuItem value={'TN'}>TN</MenuItem>
                        <MenuItem value={'TX'}>TX</MenuItem>
                        <MenuItem value={'UT'}>UT</MenuItem>
                        <MenuItem value={'VT'}>VT</MenuItem>
                        <MenuItem value={'VI'}>VI</MenuItem>
                        <MenuItem value={'VA'}>VA</MenuItem>
                        <MenuItem value={'WA'}>WA</MenuItem>
                        <MenuItem value={'WV'}>WV</MenuItem>
                        <MenuItem value={'WI'}>WI</MenuItem>
                        <MenuItem value={'WY'}>WY</MenuItem>               
                    </Select>
                </FormControl>
            </ Grid>
            <Button style={{ marginBottom: 25, marginTop: 25 }}
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                >
                Submit
            </Button>
        </form>
        <div style={{'textAlign': 'left', 'fontSize': 29, 'marginTop': 20, 'marginBottom': 20}}>
            List / <span style={{'color': '#2AABEF'}}>Representatives</span>
        </div>
        <RepsList reps={reps} />
    </div>)
}

export default RepForm;
