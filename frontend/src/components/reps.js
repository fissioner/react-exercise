import * as React from 'react';
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';


export default function RepsList({ reps }) {
    
  return (
    <Paper sx={{ width: '100%', maxWidth: 350 }}>
      <TableContainer sx={{ maxHeight: 440 }}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow sx={{backgroundColor: 'gray'}}>
                <TableCell>
                    Name
                </TableCell>
                <TableCell>
                    Party
                </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {reps.map((rep) => {
                return (
                  <TableRow hover role="checkbox" key={rep['phone']}>
                    <TableCell>
                        {rep['name']}
                    </TableCell>
                    <TableCell>
                        {rep['party'].charAt(0)}
                    </TableCell>
                  </TableRow>
                );
              })}
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
  );
}